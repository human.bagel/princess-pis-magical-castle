<?php
$logfile = "logplain.txt";
$ips = exec("uniq $logfile",$lines);
$ipfile = implode("\r\n",$lines);
$running = 'false'; // for the javascript
$toscan = 'toscan/toscan.txt';


if($_GET['execute'] == 8) {
	$running = 'true'; // for the javascript
	
	file_put_contents($toscan,$ipfile);
	exec("nmap -T4 -F -iL $toscan > results.txt&");
}

if($_GET['decree'] == 'clear') {
	file_put_contents($toscan,'');
	file_put_contents($logfile ,'');
	file_put_contents('results.txt','');
}
?>
<html>
<head>
<title>Princess Pi's IP Logger-Scanner!</title>
<style>
#results { display:none; }

body {
    font-family: Georgia,Palatino,serif;
	background: url('images/pimpslap.png');
    background-color: #FFDDDD;
}

pre {
    font-family: "Comic Sans MS",Palatino,ariel;
}

h1.title {
    color: purple;
	font-size: 3em;
}

h2 { color: purple; }

img { border: 0; }

.hidden { display: none; }

input[type=text],textarea { 
    border: 2px solid purple;
    background-color: #FFEEEE;
}

input[type=text]:focus,textarea:focus {
    border: 2px solid pink;
}

select {
    border: 2px solid purple;
    background-color: #FFEEEE;
}

select:focus {
    border: 2px solid pink;
}

input[type=button],input[type=file],input[type=submit] {
    border: 2px solid purple;
    background: #FFEEEE;
}

input[type=button]:hover,input[type=file]:hover,input[type=submit]:hover {
    background: #FFAAAA;
}
</style>
</head>
<body>
<h1 class="title">Princess Pi's IP Logger-Scanner!</h1>
<p>Find out more about your friends! You're bound to find something to talk about!</p>
<form action="" method="get">
<input type="hidden" name="execute" value="8">
<input type="submit" value="Scan IPs">
</form>
<form action="" method="get">
<input type="hidden" name="decree" value="clear">
<input type="submit" value="Clear Logs">
</form>
<p><iframe id="results" src="results.txt"></iframe></p>
<h2>Look at all these new friends! Lets find out more about them!</h2>
<pre>
<?=$ipfile;?>
</pre>
<h3>How do I use this</h3>
<p>Just embed an image pointing to https://rduke.com/public2/showalbum.php?png=spacer.png or where ever your script is! The png option can take any png image and use that.</p>
<p>Have fun Friends<3Princess Pi Loves you</p>
<script>
var running = <?=$running;?>;
var resultsBox = document.getElementById("results");
function refresh() {
	resultsBox.contentWindow.location.reload();
}

if(running) {
	setInterval(refresh,1500);
	resultsBox.style.display = "block";
}

</script>
</body>
</html>