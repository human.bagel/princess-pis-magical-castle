<?php
error_reporting(E_ALL);
ini_set('display_errors', 1); 

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
     $ip=$_SERVER['HTTP_CLIENT_IP'];
	}  
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

$SERVER = $_SERVER;
unset($SERVER['USER']);
unset($SERVER['HOME']);
unset($SERVER['SCRIPT_FILENAME']);
unset($SERVER['DOCUMENT_ROOT']);
unset($SERVER['SERVER_SOFTWARE']);
unset($SERVER['REQUEST_TIME_FLOAT']);
unset($SERVER['REQUEST_TIME']);
//unset($SERVER['RCGI_ROLE']);
unset($SERVER['FCGI_ROLE']);
unset($SERVER['GATEWAY_INTERFACE']);
unset($SERVER['PHP_SELF']);

unset($SERVER['REQUEST_URI']);
unset($SERVER['DOCUMENT_URI']);
unset($SERVER['SERVER_ADDR']);
unset($SERVER['HTTPS']);
unset($SERVER['HTTP_HOST']);
unset($SERVER['SERVER_NAME']);
unset($SERVER['SERVER_NAME']);
unset($SERVER['SERVER_PORT']);
unset($SERVER['QUERY_STRING']);
unset($SERVER['SCRIPT_NAME']);

//getRealIpAddr();
$details = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.getRealIpAddr()));
unset($details['geoplugin_credit']);

$ip = $details['geoplugin_request'];
$city = $details['geoplugin_city'];
$state = $details['geoplugin_regionName'];
$country = $details['geoplugin_countryName'];

$string = "$ip : $city : $state : $country : {$_SERVER['HTTP_USER_AGENT']}\r\n";

$plain = $details['geoplugin_request']."\r\n";

/*
$string = "$ip : $city : $state : $country\r\n\r\n\$_SERVER DUMP\r\n";
$string .= print_r($SERVER,true);
$string .= "\r\n\r\nGEOPLUGIN DUMP\r\n";
$string .= print_r($details,true) . "\r\n\r\n--------------------\r\n\r\n";

*/

file_put_contents('logimage.txt',$string,FILE_APPEND);
file_put_contents('logplain.txt',$plain,FILE_APPEND);

$img = imagecreatefrompng('images/'.$_GET['png']);
header('Content-Type: image/png');
imagepng($img);
?>