<html>
<head>
<title>Browser Side Encryption System (BSES)</title>
<link rel="stylesheet" type="text/css" href="/Access/style.css">
</head>
<body>
<div id="content">
<?php include('menu.php'); ?>
<h1>Encrypt a File with PadCipher</h1>
Upload a <a href="encrypt.php">Message File</a>
<input id="message" name="message" type="file"><br>
Upload a <a href="generate.php">CypherKey</a>
<input name="pad" id="pad" type="file"><br><br>
<input type="button" onclick="encryptFile()" value="Encrypt">
</form>


<script src="downloader.js"></script>

<script>
function encryptFile() {
    var message = document.getElementById("message");
    var pad = document.getElementById("pad");

    var padreader = new FileReader();
    var messagereader = new FileReader();

    messagereader.onload = function(){
      var messagestr = messagereader.result;
      padreader.onload = function() { var padstr = padreader.result; encrypt(messagestr,padstr); }
    };

    messagereader.readAsDataURL(message.files[0]);
    padreader.readAsText(pad.files[0]);
}

function encrypt(messagestr,padstr) {
var encodedarr = new Array();
var out = "";

    padarr = padstr.split("-");
    messagearr = messagestr.split(""); 

    for(i=0; i<messagearr.length; i++) {
      encodedarr.push(messagearr[i].charCodeAt(0));
    }

    for(i=0; i<encodedarr.length; i++) {
      out += parseInt(encodedarr[i])+parseInt(padarr[i]) + "-";
    }
out = out.slice(0,-1);
out = String.fromCharCode(out.toString());
alert(out);
download(out,"EncryptedFile.pad","text/plain");
}

</script>
</body>
</html>