<?php error_reporting(E_ALL);
ini_set('display_errors', 1); 

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
     $ip=$_SERVER['HTTP_CLIENT_IP'];
	}  
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

$SERVER = $_SERVER;
unset($SERVER['USER']);
unset($SERVER['HOME']);
unset($SERVER['SCRIPT_FILENAME']);
unset($SERVER['DOCUMENT_ROOT']);
unset($SERVER['SERVER_SOFTWARE']);
unset($SERVER['REQUEST_TIME_FLOAT']);
unset($SERVER['REQUEST_TIME']);
//unset($SERVER['RCGI_ROLE']);
unset($SERVER['FCGI_ROLE']);
unset($SERVER['GATEWAY_INTERFACE']);
unset($SERVER['PHP_SELF']);

//getRealIpAddr();
$details = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.getRealIpAddr()));

$ip = $details['geoplugin_request'];
$city = $details['geoplugin_city'];
$state = $details['geoplugin_regionName'];
$country = $details['geoplugin_countryName'];

$string = "$ip : $city : $state : $country\r\n\r\n\$SERVER DUMP\r\n";
$string .= print_r($SERVER,true);
$string .= "\r\n\r\nGEOPLUGIN DUMP\r\n";
$string .= print_r($details,true) . "\r\n\r\n--------------------\r\n\r\n";

file_put_contents('log.txt',$string,FILE_APPEND);

//echo "<pre>$string</pre>";

//echo "<div id=\"personalinfo\"><p><b>Your IP address: <i>$ip</i></b></p><p>Your location {$state}, {$country} near $city</div>";

?>